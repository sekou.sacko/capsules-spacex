import {View, Text} from "react-native";
import global from "../global/global";

/**
 * Ce composant affiche une paire de clé valeur correspondant au libellé et à la valeur de
 * chaque entrée de l'objet Capsule
 * @param label: le libellé à afficher
 * @param value: la valeur à afficher
 * @returns {JSX.Element}
 * @constructor
 */
export default function DetailItem ({label, value}) {

    return (
        <View style={global.cardRow}>
            <Text style={global.cardRowTitle}>{label}</Text>
            <Text style={global.cardRowValue}>{value}</Text>
        </View>
    )
}