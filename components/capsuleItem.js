import React from "react";
import {View, Text, TouchableOpacity} from "react-native";
import global from "../global/global";

/**
 * Ce composant affiche de façon sommaire un élément de Capsule.
 * @param capsule la capsule à afficher
 * @param navigation instance de la navigation permettant de naviguer entre les pages.
 * @returns {JSX.Element}
 * @constructor
 */
export default function CapsuleItem ({capsule, navigation}) {

    const pressHandler = () => {
        navigation.navigate('Detail', {capsule});
    }

    return (
        <View style={global.listContainer}>
            <View style={global.itemLeftContainer}>
                <Text style={global.title}>{capsule.id} - {capsule.type}</Text>
                <Text>Status: {capsule.status} - Missions: {capsule.missions.length}</Text>
            </View>
            <View style={global.itemRightContainer}>
                <TouchableOpacity onPress={pressHandler}>
                    <Text style={global.showMoreButton}>Voir plus</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}