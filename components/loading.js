import {View, Text} from "react-native";
import global from "../global/global";

/**
 * Ce composant renvoie la vue afifichant un message specifique.
 * @param message le message à afficher
 * @returns {JSX.Element}
 * @constructor
 */
export default function Loading({message}) {
    return (
        <View style={global.defaultContainer}>
            <Text style={global.messageText}>{message}</Text>
        </View>
    )
}