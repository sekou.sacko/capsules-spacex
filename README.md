# SpaceX Capsules

Cette application est développée en React-Native associé à Expo. Elle permet de :

<ol>
<li>De récupérer et afficher une liste de capsules à travers l'API REST SpaceX</li>
<li>De visualiser, à la demande, les détails d'une capsule à travers un bouton **Voir Plus**</li>
</ol>

## API SPACEX REST
**Lien de l'API** : (https://api.spacex.land/rest/capsules)


## Information du candidat
Sékou SACKO
