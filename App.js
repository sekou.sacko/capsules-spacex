import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from "./screen/Home";
import Detail from "./screen/Detail";

// La pile de navigation
const Stack = createNativeStackNavigator();

/**
 * Point d'entrée de l'application.
 * Dans cette fonction nous définition toutes page navigable de l'application et
 * définition par défaut "Home" comme la page d'accueil à travers l'attribut "initialRouteName".
 * @returns {JSX.Element}
 * @constructor
 */
export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home} options={{title: 'Capsules'}}/>
                <Stack.Screen name="Detail" component={Detail}
                              options={({route}) => ({title: 'Capsule ' + route.params.capsule.id})}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}