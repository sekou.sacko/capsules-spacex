import {StyleSheet} from "react-native";

/**
 * Ce fichier centralise tous les styles utilisés dans l'application.
 */
export default global = StyleSheet.create({
    defaultContainer: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    messageText: {
        fontSize: 16,
        fontWeight: '700'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10
    },
    listContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        marginBottom: 5,
        borderColor: '#ddd'
    },
    itemLeftContainer: {
        padding: 10,
        width: '70%'
    },
    itemRightContainer: {
        width: '30%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5'
    },
    title: {
        fontSize: 14,
        fontWeight: '700'
    },
    showMoreButton: {
        fontWeight: '700',
        textTransform: 'uppercase',
        backgroundColor: '#f5f5f5'
    },
    card: {
        borderWidth: 5,
        borderColor: '#dddddd',
        borderRadius: 15,
        marginBottom: 15
    },
    cardHeader: {
        padding: 10
    },
    cardRow: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd'
    },
    cardRowTitle: {
        textAlign: 'left',
        fontSize: 15,
        fontWeight: '700',
        color: '#aaa',
        textTransform: 'uppercase'
    },
    cardRowValue: {
        textAlign: 'right',
        fontSize: 15,
        color: '#000',
        textTransform: 'uppercase',
    },
    cardHeaderText: {
        fontSize: 14,
        fontWeight: '700',
        color: '#666',
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    missionCounter: {
        padding: 5,
        width: '50%',
        marginLeft: '25%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        borderColor: '#ddd',
        borderWidth: 1
    }
})