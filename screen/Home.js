import React, {useState, useEffect} from "react";
import {View, FlatList} from "react-native";
import global from "../global/global";
import CapsuleItem from "../components/capsuleItem";
import Loading from "../components/loading";

// URL de l'API SpaceX pour récupérer la liste des capsules.
const API_SPACEX_CAPSULES = 'https://api.spacex.land/rest/capsules';

/**
 * Affiche la page d'accueil sur laquelle s'affiche la liste des capsules.
 * @param navigation
 * @returns {JSX.Element}
 * @constructor
 */
export default function Home({navigation}) {
    const [isLoading, setLoading] = useState(true);
    const [capsules, setCapsules] = useState([]);

    console.log(capsules);

    /**
     * Récupération, en une fois, de la liste des capsules depuis le Web service SpaceX.
     */
    useEffect(() => {
        fetch(API_SPACEX_CAPSULES)
            .then((response) => response.json())
            .then((json) => setCapsules(json))
            .catch(error => console.log(error))
            .finally(() => setLoading(false))
    }, [])

    /**
     * Pendant le chargement de la liste on affiche un message d'attente
     * invitant l'utilisateur à patienter.
     */
    return (
        isLoading ? (
            <Loading message="Récupération de la liste des capsules..."/>
        ) : (
            <View style={global.container}>
                <FlatList
                    data={capsules}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => (
                        <CapsuleItem capsule={item} navigation={navigation}/>
                    )}
                />
            </View>
        )
    );
}
