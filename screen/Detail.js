import React, {useState} from "react";
import {View, Text, SafeAreaView, ScrollView} from "react-native";
import global from "../global/global";
import DetailItem from "../components/detailItem";
import moment from "moment";

export default function Detail({route}) {
    // La capsule à afficher
    const [capsule, setCapsule] = useState(route.params.capsule);

    console.log(capsule);

    // Définition de Français comme langue locale de la librairie moment.
    moment.locale('fr');
    return (
        <SafeAreaView style={global.container}>
            <ScrollView>
                <View style={global.cardHeader}>
                    <Text style={global.cardHeaderText}>Détails de la capsule {capsule.id}</Text>
                </View>
                <View style={global.card}>
                    <DetailItem label={'Id'} value={capsule.id}/>
                    <DetailItem label={'Landings'} value={capsule.landings}/>
                    <DetailItem label={'Original Launch'} value={moment(capsule.original_launch).format('DD MM YYYY à hh:mm:ss')}/>
                    <DetailItem label={'Reuse count'} value={capsule.reuse_count}/>
                    <DetailItem label={'Status'} value={capsule.status}/>
                    <DetailItem label={'Type'} value={capsule.type}/>
                    <DetailItem label={'Dragon'} value={capsule.dragon.id}/>
                </View>

                <View style={global.cardHeader}>
                    <Text style={global.cardHeaderText}>Le nombre de missions: {capsule.missions.length}</Text>
                </View>
                {/** Pour chaque mission, on affiche les informations de façon séparée*/}
                {capsule.missions.map((mission, index) => (
                    <View key={index}>
                        <View key={'head-' + mission.name} style={global.missionCounter}>
                            <Text>Mission N° {index + 1}</Text>
                        </View>
                        <View key={mission.name} style={global.card}>
                            <DetailItem label={'Name'} value={mission.name}/>
                            <DetailItem label={'Flight'} value={mission.flight}/>
                        </View>
                    </View>
                ))}
            </ScrollView>
        </SafeAreaView>
    )
}